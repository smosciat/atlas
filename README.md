# Start thin images

Assuming you have already follow the [geneal guide on thin image on the cvmfs documentation.][graph-driver-doc]

Hence, we are assuming that the docker daemon is running with the graph-driver plugin installed.

After the plugin is installed you should be able to run thin-images in your local docker.

This page show first how to mount the CVMFS repository that backs the thin images and then how to obtain the thin images themselves.

## CVMFS repository

At the moment the CVMFS repository that backs the atlas thin images life in a basic open stack machine.

We don't have stratum-1 or cached in front of the machine yet.

The repository is called `atlas.containers.cern.ch` and it is served under the address:
    http://137.138.33.45/cvmfs/atlas.containers.cern.ch
    
In order to properly mount the repository with CVMFS it is necessary to:

    1. Make CVMFS aware of it's existency
    2. Point CVMFS to the right address
    3. Provide the keys to CVMFS

### Make CVMFS aware of the thin image repository

The simplest way to do it is to add this line:

    CVMFS_REPOSITORIES=atlas.containers.cern.ch
    
To the `/etc/cvmfs/default.local` file.

    sudo echo "CVMFS_REPOSITORIES=atlas.containers.cern.ch" >> /etc/cvmfs/default.local

### Point CVMFS to the right address

Similarly CVMFS should be aware of where to find the repository and how to connect to it, so is necessary to set the URL of the repository 
and how to connect with it.

It is necessary to set the following parameters:

    CVMFS_HTTP_PROXY=DIRECT
    CVMFS_SERVER_URL=http://137.138.33.45/cvmfs/atlas.containers.cern.ch

Those parameters can be changed into the file: `/etc/cvmfs/config.d/atlas.containers.cern.ch.conf`

    sudo cat >> /etc/cvmfs/config.d/atlas.containers.cern.ch.conf << EOF 
    CVMFS_HTTP_PROXY=DIRECT
    CVMFS_SERVER_URL=http://137.138.33.45/cvmfs/atlas.containers.cern.ch
    EOF

### Provide the keys

The final step is to provide to CVMFS the keys to access the repository.

The key of the repository is

    -----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1ZpsYCMcFFYXW7KZO3JP
    agv1+GlZRnNGvFTf7fjc8vkN9DFWN4R7CvN3X8fMnFYzIZH2/9HA8yn+mfLwzrF4
    5vtHsB9PPXLFjcjXYW3giT+qZk3ohLhQeZG2epPO8AYT8T0WyOYCX1tbAftSqUh0
    HSvpJyeZW69Uaam+cQIJz549zOcEfnvVIbD1Hz5I1ULDVoKWP/ycGK54Ks7IoVpl
    /GamudfA0h68qVTYawjiCiHxx7xIaxmcQAoNrXkKOJJFF3sw2Z2dtmyPPtMhC3xg
    aFZ3oN3icmvT6kG9NQnZTyJijLG4O9FxtWBHbHYa+hjIZFUqjPQ58ZiOIzr0cewW
    rQIDAQAB
    -----END PUBLIC KEY-----

It can be saved into the file: `/etc/cvmfs/keys/atlas.containers.cern.ch.pub`


    sudo cat >> /etc/cvmfs/keys/atlas.containers.cern.ch.pub << EOF
    -----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1ZpsYCMcFFYXW7KZO3JP
    agv1+GlZRnNGvFTf7fjc8vkN9DFWN4R7CvN3X8fMnFYzIZH2/9HA8yn+mfLwzrF4
    5vtHsB9PPXLFjcjXYW3giT+qZk3ohLhQeZG2epPO8AYT8T0WyOYCX1tbAftSqUh0
    HSvpJyeZW69Uaam+cQIJz549zOcEfnvVIbD1Hz5I1ULDVoKWP/ycGK54Ks7IoVpl
    /GamudfA0h68qVTYawjiCiHxx7xIaxmcQAoNrXkKOJJFF3sw2Z2dtmyPPtMhC3xg
    aFZ3oN3icmvT6kG9NQnZTyJijLG4O9FxtWBHbHYa+hjIZFUqjPQ58ZiOIzr0cewW
    rQIDAQAB
    -----END PUBLIC KEY-----    
    EOF

### Confirm success

At this point, you can confirm probing the repository

    $ cvmfs_config probe atlas.containers.cern.ch
    > Probing /cvmfs/atlas.containers.cern.ch... OK

## Image location

The image are located in the docker registry associated with this same project.

The conversion between normal atlas docker images and the this image is the following:

| Normal docker image       | Thin docker image                                               |
|---------------------------|-----------------------------------------------------------------|
| atlas/analysisbase:latest | gitlab-registry.cern.ch/smosciat/atlas/thin/analysisbase:latest |
| atlas/athanalysis:latest  | gitlab-registry.cern.ch/smosciat/atlas/thin/athanalysis:latest  |
| atlas/athena:latest       | gitlab-registry.cern.ch/smosciat/atlas/thin/athena:latest       |

For running the thin version of `atlas/analysisbase:latest` is necessary to run:

    docker run -it gitlab-registry.cern.ch/smosciat/atlas/thin/analysisbase:latest /bin/bash

Please note the `/bin/bash` at the end of the docker command.



[graph-driver-doc]: http://cvmfs.readthedocs.io/en/stable/cpt-graphdriver.html